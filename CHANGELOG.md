# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

### Fixed

- 'function' and 'presets' now are mutually exclusive

### Changed

- Documentation: web content selection example

## [1.1.0](https://gitlab.com/ai-gimlab/mistralcli/-/releases/v1.1.0) - 2024-07-20

### Added

- Added custom `User-Agent` to request header
- `tool_call_id` API option mandatory when role is *tool*
- Better logic for relations between command line options

## [1.0.0](https://gitlab.com/ai-gimlab/mistralcli/-/releases/v1.0.0) - 2024-04-10

### First stable release

---

[Back to project main page](https://gitlab.com/ai-gimlab/mistralcli#mistralcli---overview)

---
