package main

const helpMsgUsage = "Usage: %s [OPTIONS] \"PROMPT\"\n"

const helpMsgBody = `
Terminal Chat Completion client for Mistral's AI models

Defaults: model:
  mistral-small-latest
  model embeddings: mistral-embed
  model function: mistral-large-latest
  model tool: mistral-large-latest
  function call: auto
  temperature: 0.0
  top_p: 1.0
  max response tokens: 1000
  stream response mode: false
  seed: null
  safe prompt: false
  connection timeout: 120 sec
  connection timeout stream mode (chunk, total): 5 sec, 180 sec
  number of retries: 0
  wait before retry: 10 sec
  embeddings csv filename: data.csv

Order matter:
  first OPTIONS
  then PROMPT

Notes:
  . PROMPT must always be enclosed in "double" quotes.

Online Documentation:
  <https://gitlab.com/ai-gimlab/mistralcli#mistralcli---overview>

OPTIONS:
Global:
      --defaults                  prints the program default values
  -l, --list-models               list names of all available models and exit
  -m, --model=MODEL               select model
                                  use '--list-models' to list all available models
  -p, --preview                   preview request payload and exit
      --response-raw              print full response body raw
      --response-json             print full response body json formatted
      --help                      print this help
      --version                   output version information and exit

Network:
      --retries=RETRIES           number of connection retries if network timeout occur
                                  use the '--retries-wait' option to insert a pause between retries
      --retries-wait=SECONDS      in conjunction with '--retries RETRIES' option insert a pause between retries
      --timeout=SECONDS           network connection timeout, in seconds
                                  apply to a complete request/response session
                                  SECONDS = 0 means no timeout
      --timeout-chunk=SECONDS     network connection timeout, in seconds
                                  used only in stream mode
			          apply to every streamed chunk of response

API Auth:                         ref: <https://gitlab.com/ai-gimlab/mistralcli#how-to-get-and-use-mistral-ai-api-key>
  -f, --file=APIKEY_FILE          file with Mistral AI api key (MISTRAL_API_KEY=your_apikey)

Chat/Completion/Vision API:       ref: <https://docs.mistral.ai/api/#operation/createChatCompletion>
  -a, --assistant="PROMPT"        [role]: 'assistant' message PROMPT
                                  must be used with '--previous-prompt' option
  -c, --count=SELECT              prints how many tokens have been used
                                  can also print the word count of the AI response
                                  SELECT can be 'in', 'out', 'total' (in + out) or 'words'
			          can also be any comma separated combination of above,
			          without any space, without trailing comma:
 			            in,out
			            in,out,total,words
			            etc...
      --csv=CSV_FILE              export request/response data, csv formatted, to CSV_FILE
                                   - if CSV_FILE does not exist: ask permission to create a new one
			           - if CSV_FILE exist: append data
			          no stream mode
      --format=FORMAT             the format that the model must output
                                  FORMAT can be either 'text' or 'json_object' (or simply 'json')
      --function='{JSON}'         json object containing function definition
			          must be enclosed in single quotes
			          no stream mode
			          use '--function-examples' to view how to compose json object
                                  support multiple functions
				  ref: <https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#functions>
      --function-call="MODE"      how the model responds to function calls
                                  MODE can be:
			           - "auto" (model choose if calling function)
		         	   - "none" (model does not call a function)
				   - "any" (always call function)
      --function-examples         print some examples of function json object and exit
      --list-presets              list all available predefined tasks and exit
      --preset=PRESET_NAME        predefined tasks, such as summarization, sentiment analisys, etc...
                                  use '--list-presets' to list all available PRESET_NAMEs and their purpose
      --preset-system             print PRESET_NAME predefined system message and exit
      --previous-prompt=PROMPT    in conjunction with the '--assistant' option simulates a single round of chat
                                  in conjunction with the '--tool' option simulates a single round of function call
  -r, --response-tokens=TOKENS    maximun number of response tokens
				  ref: <https://docs.mistral.ai/platform/endpoints/> for models context size
      --safe                      add a safety prompt before all conversations
                                  ref: <https://docs.mistral.ai/platform/guardrailing/#system-prompt-to-enforce-guardrails>
      --seed=NUMBER               integer number
                                  multiple requests with same prompt, seed and params should return similar/equal result
      --stream		          start printing completion before the full completion is finished
  -s, --system="PROMPT"           [role]: system message PROMPT
  -t, --temperature=VALUE         VALUE range from 0.0 to 1.0
      --tool-args='{JSON}'        json object containing function arguments returned by a model function call
			          must be enclosed in single quotes
                                  must be used in conjunction with '--previous-prompt', '--tool-name' and '--tool-content' options
				  ref: <https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#tool>
      --tool-callid=ID            function ID returned by a model function call
                                  must be used in conjunction with '--previous-prompt' and '--tool-name' options
				  ref: <https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#tool>
      --tool-content='{JSON}'     json object containing result from user application function
			          must be enclosed in single quotes
                                  must be used in conjunction with '--previous-prompt' and '--tool-name' options
				  ref: <https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#tool>
      --tool-name="NAME"          function NAME returned by a model function call
                                  must be used in conjunction with '--previous-prompt' option
				  ref: <https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#tool>
      --top-p=VALUE               top_p: VALUE range from 0.0 to 1.0
      --web=URL                   any web page URL
			          URL must always be encloded in single or double quotes
			          in user PROMPT ask only for the context of the page. Eg.:
			           - WRONG: mistralcli --web "https://www.example.com" "From the following web page {QUESTION}"
			           - OK:    mistralcli --web "https://www.example.com" "Extract a list of cat names"
			          used in conjunction with option '--preset', URL content becomes the user PROMPT. Eg:
			           - mistralcli --preset ner --web "https://www.example.com"
			             execute NER preset using content from "https://www.example.com" web page
      --web-select=SELECTOR       select a section of web page, then strip html tags
                                  SELECTOR can be any HTML element, such as 'h2', 'p', 'body', 'article', etc...
                                  SELECTOR can also be any class or id attribute (eg.: '.myClass', '#myID')
			          lastly SELECTOR can be 'NONE' keyword: no html tag stripping
			           - Note: NONE can use a lot of tokens
			          SELECTOR must always be encloded in single or double quotes
			          default selectors are 'main', if exist, or 'body'
      --web-test                  print an output of text extracted from web page and exit
                                  useful to find the right HTML selector (check --web-select option)
			          with the right SELECTOR less tokens are used

Embeddings API:                   ref: <https://docs.mistral.ai/api/#operation/createEmbedding>
  -e, --embed                     return PROMPT embedding
                                  PROMPT must always be enclosed in "double" quotes
				  multiple PROMPTs must be separated by double commas
				  eg: mistralcli --embed "PROMPT_1,,PROMPT_2,,...,,PROMPT_N"
				  return a cvs table with columns 'id, text, embedding'
				  append table to file 'data.csv' or create new one
				   - to save in a different file use '--csv FILENAME' option
				   - in conjunction with the '--response-json' or '--response-raw' options
				     response is not saved to file, instead output json data to stdout
`

const helpMsgTips = "  💡                              %s --count in,out,total \"Summarize the following text: {TEXT}\"\n"

// befor this var, always print cmd name, without newline char (os.Args[0]). eg: fmt.Fprintf(os.Stdout, "%s", os.Args[0]); fmt.Println(verMsg)
const verMsg = ` (gimlab) 1.1.2
Copyright (C) 2024 gimlab.
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.`

const jsonPayloadExamples = `[FUNCTION JSON OBJECTS EXAMPLES]

// --- one line --- //
{"name":"retrieve_payment_status","description":"Get payment status of a transaction","parameters":{"type":"object","properties":{"transaction_id":{"type":"string","description":"The transaction id."}},"required":["transaction_id"]}}

// --- pretty print --- //
{
  "name": "retrieve_payment_status",
  "description": "Get payment status of a transaction",
  "parameters": {
    "type": "object",
    "properties": {
      "transaction_id": {
        "type": "string",
        "description": "The transaction id."
      }
    },
    "required": [
      "transaction_id"
    ]
  }
}

// --- tips --- //
[LINUX]
If you are using a Linux terminal with Bash, follow these steps:

1. create JSON file, eg. function.json, with your data, formatted as the examples above.
2. create a var to hold json data. Eg. if var name is "FUNC" and filename is "function.json": 
   export FUNC=$(cat function.json)
3. use your var with command:
   mistralcli --function "$FUNC" "PROMPT"

If you prefer, create json file, as in step 1, and skip steps 2, 3 and 4. Then:
   mistralcli --function "$(cat function.json)" "PROMPT"

Whichever method you choose, always remember the double quotes:
"$FUNC", "$(cat function.json)", "PROMPT", etc...

[WINDOWS]
If you are using Windows with CMD shell, follow these steps:

1. create JSON file, eg. function.json, with your data, formatted as the examples above.
2. all double quotes in your json data must be escaped with backslash (\). Eg:

{\"name\":\"retrieve_payment_status\",\"description\":\"Get payment status of a transaction\",\"parameters\":{\"type\":\"object\",\"properties\":{\"transaction_id\":{\"type\":\"string\",\"description\":\"The transaction id.\"}},\"required\":[\"transaction_id\"]}}

3. create a var to hold json data. Eg. if var name is "FUNC" and filename is "function.json": 
   FOR /F "tokens=*" %g IN ('type function.json') do (SET FUNC=%g)
4. use your var with command:
   mistralcli.exe --function "%FUNC%" "PROMPT"

Whichever method you choose, always remember the single or double quotes:
"tokens=*", 'type function.json', "%FUNC%" "PROMPT", etc...

// --- notes --- //
- The same tips can be applied for all PROMPTs (user, system, etc...).
- For more advanced functions usage check the online documentation:
  https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#functions
`

const defaultValues string = `Defaults:
  model: mistral-small-latest
  model embeddings: mistral-embed
  model function: mistral-large-latest
  model tool: mistral-large-latest
  function call: auto
  temperature: 0.0
  top_p: 1.0
  max response tokens: 1000
  stream response mode: false
  seed: null
  safe prompt: false
  connection timeout: 120 sec
  connection timeout stream mode (chunk, total): 5 sec, 180 sec
  number of retries: 0
  wait before retry: 10 sec
  embeddings csv filename: data.csv`
