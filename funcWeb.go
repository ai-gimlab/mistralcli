package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httputil"
	"os"
	"strings"
	"time"

	"github.com/joho/godotenv"
)

// create and send HTTP request
// exctract response body (no stream mode) or call stream function 'httpReceiveStream()'
// func httpRequestPOST(payloadJSON []byte, endpoint string) (bodyString string, err error) {
func httpRequestPOST(payloadJSON []byte) (bodyString string, err error) {
	// function name to return if any error occur
	var funcName string = "httpRequestPOST"

	// load env configs
	key, err := loadEnv()
	if err != nil {
		return "", err
	}

	// get api key
	var bearer string = "Bearer " + key
	var endpoint string

	// select endpoint: 'completion' or 'embeddings'
	switch {
	case flagEmbed:
		endpoint = urlEmbed
	default:
		endpoint = urlCompletion
	}

	// create and configure HTTP request
	req, err := http.NewRequest("POST", endpoint, bytes.NewBuffer(payloadJSON))
	if err != nil {
		return "", fmt.Errorf("func %q - error creating HTTP request: %w\n", funcName, err)
	}

	// set HTTP User-Agent Header
	ua, err := createUserAgent(req)
	if err != nil {
		return "", fmt.Errorf("func %q - %w\n", funcName, err)
	}

	// add the necessary HTTP headers
	req.Header.Add("Authorization", bearer)
	req.Header.Add("Content-Type", "application/json; charset=UTF-8")
	req.Header.Add("User-Agent", ua)

	// create HTTP client
	client := &http.Client{
		Timeout: timeout,
	}

	// make HTTP request
	startTime := time.Now()
	resp, errResp := client.Do(req)
	endTime := time.Now()
	if errResp != nil {
		e := strings.ReplaceAll(errResp.Error(), "\"", "'")
		return "", fmt.Errorf("func '%s' - error during POST request: %s\n", funcName, e)
	}
	defer resp.Body.Close()

	// encode elapsed time to csv
	elapsed := endTime.Sub(startTime)
	csvFields[requestDateTime] = startTime.Format(time.DateTime)
	csvFields[responseElapsedTime] = fmt.Sprintf("%.9f", float64(elapsed)/nanosec)

	// check HTTP response status
	if resp.StatusCode != http.StatusOK {
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return "", fmt.Errorf("func %q - error reading HTTP response body: %w\n", funcName, err)
		}
		ppJSON, err := prettyPrintJsonString(string(body))
		errDetails := fmt.Sprintf("// ------ ERROR DETAILS ------ //\n%s", ppJSON)

		//return "", fmt.Errorf("func %q - there was an error during HTTP transaction\n\n%s", funcName, errDetails)
		return "", fmt.Errorf("func %q - error during HTTP transaction: %s\n\n%s\n", funcName, resp.Status, errDetails)
	}

	if flagStream {
		reader := bufio.NewReader(resp.Body)
		if err := httpReceiveStream(reader); err != nil {
			return "", err
		}
		return "", nil
	}

	// parse HTTP response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("func %q - error reading HTTP response body: %w\n", funcName, err)
	}

	return string(body), nil
}

func httpRequestGET(urlAddr string, requiretAuth bool) (responseBody string, err error) {
	// function name to return if any error occur
	var funcName string = "httpRequestGET"

	// load env configs
	key, err := loadEnv()
	if err != nil {
		return "", err
	}

	// set api key
	var bearer string = "Bearer " + key

	// create HTTP client
	client := &http.Client{
		Timeout: timeout,
	}

	// configure HTTP client
	req, err := http.NewRequest("GET", urlAddr, nil)
	if err != nil {
		return "", fmt.Errorf("func %q - error creating HTTP request: %w\n", funcName, err)
	}

	// set HTTP User-Agent Header
	ua, err := createUserAgent(req)
	if err != nil {
		return "", fmt.Errorf("func %q - %w\n", funcName, err)
	}
	req.Header.Add("User-Agent", ua)

	// check if any auth is required
	if requiretAuth {
		req.Header.Add("Authorization", bearer)
	}

	// make HTTP request
	resp, err := client.Do(req)
	if err != nil {
		return "", fmt.Errorf("func %q - error during HTTP transaction: %w\n", funcName, err)
	}
	defer resp.Body.Close()

	// extract body from response
	bodyString, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("func %q - error reading HTML body: %w\n", funcName, err)
	}

	return string(bodyString), nil
}

/*
 */

// parse 'STREAM mode' response
func httpReceiveStream(reader *bufio.Reader) error {
	// function name to return if any error occur
	var funcName string = "httpReceiveStream"

	//var streamOutputTokens int

	var b strings.Builder // build stream response for stream words conuting purpose

	for {
		// Set a read timeout for each iteration
		ctx, cancel := context.WithTimeout(context.Background(), timeoutChunk)
		defer cancel()

		select {
		case <-ctx.Done():
			return fmt.Errorf("func %q - timeout reading response body\n", funcName)
		default:
			// parse io reader
			line, err := reader.ReadBytes('\n')
			if err != nil {
				// check end of stream
				if errors.Is(err, io.EOF) {
					outputWordsCount = len(strings.Fields(b.String())) // count output words
					return nil
				}
				return fmt.Errorf("func %q - error reading HTTP response body: %w\n", funcName, err)
			}

			// print raw response (if '--raw' option was given)
			if flagRaw {
				fmt.Printf("%s", string(line))
				continue
			}

			// sanitize response
			data := bytes.TrimLeft(line, "data: ")
			data = bytes.TrimRight(data, "\n")

			// check if 'data' contain any data (empty lines)
			if len(data) <= 1 {
				continue
			}

			// check for last chunk
			if strings.Contains(string(data), "DONE") {
				fmt.Println()
				continue
			}

			// print response chunk json formatted
			if flagJson {
				j, err := prettyPrintJsonString(string(data))
				if err != nil {
					return err
				}
				fmt.Printf("%s\n", j)
				continue
			}

			// --- parse response
			var response responseStream
			err = json.Unmarshal(data, &response)
			if err != nil {
				return fmt.Errorf("func %q - error extracting JSON response: %w\n", funcName, err)
			}
			if len(response.Choices) > 0 && response.Choices[0].Delta.Role != assistant {
				// build response string for words counting purpose
				if _, err := b.WriteString(response.Choices[0].Delta.Content); err != nil {
					return fmt.Errorf("func %q - error building stream response\n", funcName)
				}

				// output char by char, for smooth streaming
				for _, c := range response.Choices[0].Delta.Content {
					fmt.Printf("%c", c)
					time.Sleep(streamCharsPause)
				}

				// token count
				if response.Choices[0].FinishReason == "stop" {
					streamTokens[inputTokens] = response.Usage.PromptTokens
					streamTokens[outputTokens] = response.Usage.CompletionTokens
					streamTokens[totalTokens] = response.Usage.TotalTokens
				}

				continue
			}
		}
		cancel()
	}
}

// load API key
func loadEnv() (key string, err error) {
	// function name to return if any error occur
	var funcName string = "loadEnv"

	// get env
	switch {
	case flagFile != "":
		// from supplied .env file
		err := godotenv.Load(flagFile)
		if err != nil {
			return "", fmt.Errorf("func %q - error loading env file %s", funcName, flagFile)
		}
	default:
		// from os environment
		envVar := os.Getenv(env)

		// from default .env file
		cfgEnv := os.Getenv(cfgRootDir) + cfgDir + cfgFile
		errFile := godotenv.Load(cfgEnv)

		// check result
		if errFile != nil && envVar == "" {
			return "", fmt.Errorf("func %q - error loading default env file or missing env var %q", funcName, env)
		}
	}

	// load env configs
	key, ok := os.LookupEnv(env)
	if !ok || key == "" {
		return "", fmt.Errorf("func %q - Mistral AI API KEY not given", funcName)
	}
	return key, nil
}

// createUserAgent return a custom 'User-Agent'
func createUserAgent(req *http.Request) (ua string, err error) {
	// function name to return if any error occur
	var funcName string = "createUserAgent"

	// load default Go User-Agent
	dump, err := httputil.DumpRequestOut(req, true)
	if err != nil {
		return "", fmt.Errorf("func %q - error retrieving Go default User-Agent: %w\n", funcName, err)
	}
	_, d, _ := strings.Cut(string(dump), "User-Agent: ")
	d, _, _ = strings.Cut(d, "\r\n")

	// create custom UA
	ua = fmt.Sprintf("%s (%s)", customUA, d)

	return ua, nil
}
