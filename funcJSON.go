package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"sort"
	"strings"
)

// create payloads
func createPayloadJSON() (payloadJSON []byte, err error) {
	// function name to return if any error occur
	var funcName string = "createPayloadJSON"

	// configure model request
	switch {
	case flagEmbed:
		// embeddings
		payload := &requestEmbed{
			Input:          inputs,
			Model:          modelEmbed,
			EncodingFormat: embedEncodingFmt,
		}

		// create JSON payload
		payloadJSON, err = json.Marshal(payload)
		if err != nil {
			return nil, fmt.Errorf("func %q - error creating embeddings JSON payload: %w\n", funcName, err)
		}
		return payloadJSON, nil
	default:
		// completion task, tool, function
		payload := &request{
			Model:       model,
			Messages:    messages,
			Temperature: temperature,
			TopP:        top_p,
			MaxTokens:   maxTokens,
			Stream:      flagStream,
			SafeMode:    flagSafeMode,
		}

		// random seed
		if flagSeed > 0 {
			payload.RandomSeed = flagSeed
		}

		// response format (either 'text' or 'json_object')
		if responseFormat != defaultResponseFormat {
			var r respFormat
			r.Type = responseFormat
			payload.ResponseFormat = &r
		}

		// functions
		if flagFunction != "" {
			payload.Model = modelFunction
			payload.ToolChoice = functionCall

			// parse functions list
			var tools []toolConfig
			for _, f := range functionList {
				// check if string is a valid json object
				if err := isValidJSON(f); err != nil {
					return nil, fmt.Errorf("func %q - invalid 'function' JSON format: %w\nJSON data: %s\n", funcName, err, f)
				}
				// create object to store function
				var t toolConfig
				t.Type = toolType
				// unmarshal single function object
				if err := json.Unmarshal([]byte(f), &t.Function); err != nil {
					return nil, fmt.Errorf("func %q - invalid 'function' data: %s\n", funcName, flagFunction)
				}
				// append function object to 'tools' (json) array
				tools = append(tools, t)
			}

			// add functions to payload
			payload.Tools = tools
		}

		// role tool
		if flagToolName != "" {
			payload.Model = modelTool
		}

		// create JSON payload
		payloadJSON, err = json.Marshal(payload)
		if err != nil {
			return nil, fmt.Errorf("func %q - error creating completion JSON payload: %w\n", funcName, err)
		}

		// encode request data to csv
		if flagCsv != "" && payload != nil {
			requestCSV(payload)
		}
	}

	return payloadJSON, nil
}

// pretty print one line JSON
func prettyPrintJsonString(jsonString string) (formattedString string, err error) {
	// function name to return if any error occur
	var funcName string = "prettyPrintJsonString"

	var buf bytes.Buffer
	if err := json.Indent(&buf, []byte(jsonString), "", "  "); err != nil {
		return "", fmt.Errorf("func %q - error formatting JSON response: %w\n", funcName, err)
	}

	return buf.String(), nil
}

// check for valid JSON format
func isValidJSON(data string) error {
	var temp interface{}
	err := json.Unmarshal([]byte(data), &temp)
	return err
}

func listModels() error {
	// function name to return if any error occur
	var funcName string = "listModels"

	var modelsListData ModelsList

	// request models list
	models, err := httpRequestGET(urlModelsList, true)
	if err != nil {
		return err
	}

	// validate response
	if err := isValidJSON(models); err != nil {
		return err
	}

	// decode response
	if err := json.Unmarshal([]byte(models), &modelsListData); err != nil {
		return fmt.Errorf("func %q - error extracting JSON response: %w\n", funcName, err)
	}

	// extract and print data
	if len(modelsListData.Data) > 0 {
		// sort and filter models name
		for _, m := range modelsListData.Data {
			modelNames = append(modelNames, m.Id)
		}

		sort.SliceStable(modelNames, func(i, j int) bool { return modelNames[i] < modelNames[j] })

		// this function can also be used by 'checkModelName()' function
		if !flagListModels {
			return nil
		}

		fmt.Println("chat/completion models:")
		for _, m := range modelNames {
			if !strings.Contains(m, "embed") {
				fmt.Printf("  %s\n", m)
			}
		}
		fmt.Println()
		fmt.Println("embedding models:")
		for _, m := range modelNames {
			if strings.Contains(m, "embed") {
				fmt.Printf("  %s\n", m)
			}
		}
		fmt.Printf("\n%s\n", modelsTechRef)
	} else {
		fmt.Printf("models data not available - retry later\n")
	}
	/*
	 */

	return nil
}
