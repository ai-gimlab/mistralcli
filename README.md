# mistralcli - overview

---

## Table of Contents

- [CHANGELOG](https://gitlab.com/ai-gimlab/mistralcli/-/blob/main/CHANGELOG.md#changelog)
- [EXAMPLES](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#mistralcli---examples)
- [Introduction](#introduction)
  - [Presets](#presets)
  - [Models](#models)
- [Disclaimer](#disclaimer)
- [Getting started](#getting-started)
  - [Binary](#binary)
  - [Compile from source](#compile-from-source)
  - [Default settings](#default-settings)
  - [Usage](#usage)
    - [Informational options](#informational-options)
    - [Data analysis options](#data-analysis-options)
- [Token count](#token-count)
- [How to get and use Mistral AI Api Key](#how-to-get-and-use-mistral-ai-api-key)
- [Credits](#credits)

---

## Introduction

mistralcli: Terminal Chat Completion client for [Mistral AI's](https://mistral.ai/technology/#models) LLM models written in [Go](https://go.dev/).

This utility is a kind of Swiss Army knife for AI developers used to working in a text-based terminal.

It has no chat features. It use chat endpoints as *completion* endpoint. Its sole purpose is to quickly craft prompts, check payload and/or response format, tokens usage, collect and analyze data, try the same prompt with different parameter settings, and so on.

Prompts can be content from web sites ([technical details](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#web-contents)). **Note**: [dynamic web pages](https://en.wikipedia.org/wiki/Dynamic_web_page) are not supported.

Can get embedding of prompt ([technical details](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#embedding)).

Embeddings can be used with companion utility [eucos](https://gitlab.com/ai-gimlab/eucos#eucos).

Request/Response session can be exported in a CSV file ([technical details](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#export-to-csv)).

More **usage examples** [here](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#mistralcli---examples).

All of the [examples](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#mistralcli---examples) in this guide use the Bash shell on Linux systems, but can run on other platforms as well.

To follow this guide a basic knowledge of [Mistral AI APIs](https://docs.mistral.ai/api/) is required.

---

### Presets

There are some basic [presets](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#presets), such as summarization or sentiment analisys, that can be used as a reference or starting point.

[**Presets**](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#presets) imply that all parameters are presetted, but they can be changed ([technical details](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#customize-presets)).

**Any preset is executed against a given prompt**. For example, preset *summary* summarize user prompt, preset *ner* execute a [Named Entity Recognition](https://en.wikipedia.org/wiki/Named-entity_recognition) task on user text prompt, etc...

Presets can also be used with web pages. If you use `--web URL` option with a preset, **web page content becomes the prompt** ([technical details](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#using-presets-with-web-content)).

#### Presets list

To list all presets use `--list-presets` option:

```bash
mistralcli --list-presets
```

Output:

```text
presets:
   baby                         (write stories for kids using user PROMPT as inspiration)
   contrarian                   (try to find any flaw in PROMPT, eg. a marketing plan, a poem, source code, etc...)
   headline                     (an headline of user PROMPT)
   ner                          (Named Entity Recognition of user PROMPT)
   offensive                    (user PROMPT analisys for offenses directed 'at': ignore bad words used as break-in)
   presentationbare             (converts PROMPT to barebone presentation - can be used as PROMPT for 'presentationcode' preset)
   presentationcode             (converts PROMPT from 'presentationbare' preset to Python code needed to create a PowerPoint basic presentation)
   sentiment                    (sentiment analisys of user PROMPT: positive/negative)
   sentimentneutral             (sentiment analisys of user PROMPT: positive/negative/neutral)
   summary                      (summary of user PROMPT)
   summarybrief                 (brief summary of user PROMPT)
   summarybullet                (bulleted summary of user PROMPT)
   table                        (try to find a data pattern from PROMPT, to output as table)
   tablecsv                     (try to find a data pattern from PROMPT, to output as table, CSV formatted)
   tutor                        (give a big picture about PROMPT topic)

  💡                            mistralcli --preset summarybullet --stream --web "https://www.examples.com"
```

More **presets usage examples** [here](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#presets).

---

### Models

To list all supported Mistral AI's chat completion models, use `--list-models` option:

```bash
# actual query to AI model
mistralcli --list-models
```

Output:

```txt
chat/completion models:
  codestral-2405
  codestral-latest
  codestral-mamba-2407
  codestral-mamba-latest
  mistral-large-2402
  mistral-large-latest
  mistral-medium
  mistral-medium-2312
  mistral-medium-latest
  mistral-small
  mistral-small-2312
  mistral-small-2402
  mistral-small-latest
  mistral-tiny
  mistral-tiny-2312
  open-codestral-mamba
  open-mistral-7b
  open-mistral-nemo
  open-mistral-nemo-2407
  open-mixtral-8x22b
  open-mixtral-8x22b-2404
  open-mixtral-8x7b

embedding models:
  mistral-embed

ref: https://docs.mistral.ai/platform/endpoints
```

With option `-m, --model` it's possible to choose which model to use.

**Note**: your models list may be different.

---

[top](#table-of-contents)

---

## Disclaimer

I enjoy to develop this utilities just for my personal use. So, use them at your own risk.

If you want to develop your own solution written in [Go](https://go.dev/) and based on Mistral AI's models, I suggest you use [mistral-go](https://github.com/Gage-Technologies/mistral-go) library. For other languages check the [Mistral AI official documentation](https://docs.mistral.ai/platform/client/).

---

[top](#table-of-contents)

---

## Getting started

### Binary

Prebuilt binary packages for Linux, Windows and MacOS can be downloaded from [here](https://gitlab.com/ai-gimlab/mistralcli/-/releases).

---

### Compile from source

If you prefer, clone this repo and compile from sources.

Prerequisite: [Go Development Environment](https://go.dev/dl/) installed.

Clone this repo:

```bash
git clone https://gitlab.com/ai-gimlab/mistralcli.git
cd mistralcli
go mod init mistralcli && go mod tidy
```

Modify tokenizer library:

- edit file: `$(go env GOPATH)/pkg/mod/github.com/sugarme/tokenizer@v0.2.2/init.go`
- comment out `CacheDir` logging from `init` function: `log.Printf("INFO: CachedDir=%q\n", CachedDir)`

**FROM**

```go
func init() {
	// default path: {$HOME}/.cache/tokenizer
	homeDir := os.Getenv("HOME")
	CachedDir = fmt.Sprintf("%s/.cache/tokenizer", homeDir)

	initEnv()

  // ****** COMMENT OUT THE FOLLOWING LINE ****** //
  log.Printf("INFO: CachedDir=%q\n", CachedDir)
}
```

**TO**

```go
func init() {
	// default path: {$HOME}/.cache/tokenizer
	homeDir := os.Getenv("HOME")
	CachedDir = fmt.Sprintf("%s/.cache/tokenizer", homeDir)

	initEnv()

  // ****** MODIFIED LINE ****** //
  //log.Printf("INFO: CachedDir=%q\n", CachedDir)
}
```

Build:

```bash
go build .
```

---

### Default settings

- **model**: mistral-small-latest
- **model embeddings**: mistral-embed
- **model function**: mistral-large-latest
- **model tool**: mistral-large-latest
- **function call**: auto
- **temperature**: 0.0
- **top_p**: 1.0
- **max response tokens**: 1000
- **stream response mode**: false
- **seed**: null
- **safe prompt**: false
- **connection timeout**: 120 sec
- **connection timeout stream mode (chunk, total)**: 5 sec, 180 sec
- **number of retries**: 0
- **wait before retry**: 10 sec
- **embeddings csv filename**: data.csv

[Presets](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#presets) have their own default settings. Use option `--preview` to view settings for a given preset ([technical details](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#preset-settings)).

---

### Usage

Basically, a [prompt is enough](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#quick-start):

```bash
mistralcli "PROMPT"
```

More **usage examples** [here](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#mistralcli-examples).

---

#### Help

Use `--help` to view all options:

```bash
mistralcli --help
```

Output:

```txt
sage: mistralcli [OPTIONS] "PROMPT"

Terminal Chat Completion client for Mistral's AI models

Defaults: model:
  mistral-small-latest
  model embeddings: mistral-embed
  model function: mistral-large-latest
  model tool: mistral-large-latest
  function call: auto
  temperature: 0.0
  top_p: 1.0
  max response tokens: 1000
  stream response mode: false
  seed: null
  safe prompt: false
  connection timeout: 120 sec
  connection timeout stream mode (chunk, total): 5 sec, 180 sec
  number of retries: 0
  wait before retry: 10 sec
  embeddings csv filename: data.csv

Order matter:
  first OPTIONS
  then PROMPT

Notes:
  . PROMPT must always be enclosed in "double" quotes.

Online Documentation:
  <https://gitlab.com/ai-gimlab/mistralcli#mistralcli---overview>

OPTIONS:
Global:
      --defaults                  prints the program default values
  -l, --list-models               list names of all available models and exit
  -m, --model=MODEL               select model
                                  use '--list-models' to list all available models
  -p, --preview                   preview request payload and exit
      --response-raw              print full response body raw
      --response-json             print full response body json formatted
      --help                      print this help
      --version                   output version information and exit

Network:
      --retries=RETRIES           number of connection retries if network timeout occur
                                  use the '--retries-wait' option to insert a pause between retries
      --retries-wait=SECONDS      in conjunction with '--retries RETRIES' option insert a pause between retries
      --timeout=SECONDS           network connection timeout, in seconds
                                  apply to a complete request/response session
                                  SECONDS = 0 means no timeout
      --timeout-chunk=SECONDS     network connection timeout, in seconds
                                  used only in stream mode
                                  apply to every streamed chunk of response

API Auth:                         ref: <https://gitlab.com/ai-gimlab/mistralcli#how-to-get-and-use-mistral-ai-api-key>
  -f, --file=APIKEY_FILE          file with Mistral AI api key (MISTRAL_API_KEY=your_apikey)

Chat/Completion/Vision API:       ref: <https://docs.mistral.ai/api/#operation/createChatCompletion>
  -a, --assistant="PROMPT"        [role]: 'assistant' message PROMPT
                                  must be used with '--previous-prompt' option
  -c, --count=SELECT              prints how many tokens have been used
                                  can also print the word count of the AI response
                                  SELECT can be 'in', 'out', 'total' (in + out) or 'words'
                                  can also be any comma separated combination of above,
                                  without any space, without trailing comma:
                                    in,out
                                    in,out,total,words
                                    etc...
      --csv=CSV_FILE              export request/response data, csv formatted, to CSV_FILE
                                   - if CSV_FILE does not exist: ask permission to create a new one
                                   - if CSV_FILE exist: append data
                                  no stream mode
      --format=FORMAT             the format that the model must output
                                  FORMAT can be either 'text' or 'json_object' (or simply 'json')
                                  supported models: <https://docs.mistral.ai/platform/changelog/>
      --function='{JSON}'         json object containing function definition
                                  must be enclosed in single quotes
                                  no stream mode
                                  use '--function-examples' to view how to compose json object
                                  support multiple functions
                                  ref: <https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#functions>
      --function-call="MODE"      how the model responds to function calls
                                  MODE can be:
                                   - "auto" (model choose if calling function)
                                   - "none" (model does not call a function)
                                   - "any" (always call function)
      --function-examples         print some examples of function json object and exit
      --list-presets              list all available predefined tasks and exit
      --preset=PRESET_NAME        predefined tasks, such as summarization, sentiment analisys, etc...
                                  use '--list-presets' to list all available PRESET_NAMEs and their purpose
      --preset-system             print PRESET_NAME predefined system message and exit
      --previous-prompt=PROMPT    in conjunction with the '--assistant' option simulates a single round of chat
                                  in conjunction with the '--tool' option simulates a single round of function call
  -r, --response-tokens=TOKENS    maximun number of response tokens
                                  ref: <https://docs.mistral.ai/platform/endpoints/> for models context size
      --safe                      add a safety prompt before all conversations
                                  ref: <https://docs.mistral.ai/platform/guardrailing/#system-prompt-to-enforce-guardrails>
      --seed=NUMBER               integer number
                                  multiple requests with same prompt, seed and params should return similar/equal result
      --stream                    start printing completion before the full completion is finished
  -s, --system="PROMPT"           [role]: system message PROMPT
  -t, --temperature=VALUE         VALUE range from 0.0 to 1.0
      --tool-args='{JSON}'        json object containing function arguments returned by a model function call
                                  must be enclosed in single quotes
                                  must be used in conjunction with '--previous-prompt', '--tool-name' and '--tool-content' options
                                  ref: <https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#tool>
      --tool-callid=ID            function ID returned by a model function call
                                  must be used in conjunction with '--previous-prompt' and '--tool-name' options
                                  ref: <https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#tool>
      --tool-content='{JSON}'     json object containing result from user application function
                                  must be enclosed in single quotes
                                  must be used in conjunction with '--previous-prompt' and '--tool-name' options
                                  ref: <https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#tool>
      --tool-name="NAME"          function NAME returned by a model function call
                                  must be used in conjunction with '--previous-prompt' option
                                  ref: <https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#tool>
      --top-p=VALUE               top_p: VALUE range from 0.0 to 1.0
      --web=URL                   any web page URL
                                  URL must always be encloded in single or double quotes
                                  in user PROMPT ask only for the context of the page. Eg.:
                                   - WRONG: mistralcli --web "https://www.example.com" "From the following web page {QUESTION}"
                                   - OK:    mistralcli --web "https://www.example.com" "Extract a list of cat names"
                                  used in conjunction with option '--preset', URL content becomes the user PROMPT. Eg:
                                   - mistralcli --preset ner --web "https://www.example.com"
                                     execute NER preset using content from "https://www.example.com" web page
      --web-select=SELECTOR       select a section of web page, then strip html tags
                                  SELECTOR can be any HTML element, such as 'h2', 'p', 'body', 'article', etc...
                                  SELECTOR can also be any class or id attribute (eg.: '.myClass', '#myID')
                                  lastly SELECTOR can be 'NONE' keyword: no html tag stripping
                                   - Note: NONE can use a lot of tokens
                                  SELECTOR must always be encloded in single or double quotes
                                  default selectors are 'main', if exist, or 'body'
      --web-test                  print an output of text extracted from web page and exit
                                  useful to find the right HTML selector (check --web-select option)
                                  with the right SELECTOR less tokens are used

Embeddings API:                   ref: <https://docs.mistral.ai/api/#operation/createEmbedding>
  -e, --embed                     return PROMPT embedding
                                  PROMPT must always be enclosed in "double" quotes
                                  multiple PROMPTs must be separated by double commas
                                  eg: mistralcli --embed "PROMPT_1,,PROMPT_2,,...,,PROMPT_N"
                                  return a cvs table with columns 'id, text, embedding'
                                  append table to file 'data.csv' or create new one
                                   - to save in a different file use '--csv FILENAME' option
                                   - in conjunction with the '--response-json' or '--response-raw' options
                                     response is not saved to file, instead output json data to stdout

  💡                              mistralcli --count in,out,total "Summarize the following text: {TEXT}"
```

---

#### Informational options

Some options output informations only, without making any requests:

- `--function-examples`: print an example of function json object
- `-l, --list-models`: list names of all available models
- `--list-presets`: list all available predefined tasks
- `--preset-system`: print predefined system message for a given preset - must be used with `--preset PRESET_NAME` option
- `-p, --preview`: preview request payload, json formatted
- `--web-test`: print an output of text extracted from web page - must be used with `--web URL` option
- `--help`: print help
- `--version`: print version information

In the following example some [parameters](https://docs.mistral.ai/api/) are set, such as temperature (`--temperature 0.8`) and system message (`--system "be polite"`). Using `-p, --preview` option, you can verify that the resulting payload is the one you want, but without performing any requests:

```bash
# preview payload
mistralcli --preview --temperature 0.8 --system "be polite" "In about 15 words write a poem about a little white cat"
```

Output:

```json
{
  "messages": [
    {
      "role": "system",
      "content": "be polite"
    },
    {
      "role": "user",
      "content": "In about 15 words write a poem about a little white cat"
    }
  ],
  "model": "mistral-small-latest",
  "temperature": 0.8,
  "top_p": 1,
  "max_tokens": 1000,
  "stream": false,
  "safe_prompt": false
}
```

---

#### Data analysis options

Some options give more verbose glimpse of what's going on during requests:

- `-c, --count`: prints how many tokens have been used - check [Token count](#token-count) section for more
- `--csv`: export request and response data, csv formatted, to file - useful for data analysis ([technical details](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#export-to-csv))
- `--response-json`: print full response, json formatted - useful to find the right key to unmarshal
- `--response-raw`: print the unformatted raw response - useful to understand how stream behave

The same example as above, but without `-p, --preview` option, because we want actually execute request. This time we use `--response-json` option, to view all response details:

```bash
# actual query to AI model
mistralcli --response-json --temperature 0.8 --system "be polite" "In about 15 words write a poem about a little white cat"
```

Response:

```json
{
  "id": "8b992144d65346d8a565beb62f0bb035",
  "object": "chat.completion",
  "created": 1711290496,
  "model": "mistral-small-latest",
  "choices": [
    {
      "index": 0,
      "message": {
        "role": "assistant",
        "content": "\"Tiny paws, soft and light,\nWhite fur gleams in moonlight,\nPlayful purrs, eyes wide bright,\nLittle white cat, pure delight.\"",
        "tool_calls": null
      },
      "finish_reason": "stop",
      "logprobs": null
    }
  ],
  "usage": {
    "prompt_tokens": 21,
    "total_tokens": 59,
    "completion_tokens": 38
  }
}
```

The AI answer contains all the information you need to understand what's going on under the hood:

- `"usage":` contain actual tokens usage
- `"choices":` contain responses
- `"model":` contain which model was used at endpoint side
- etc...

More **details on** [Mistral AI API documentation](https://docs.mistral.ai/api/) page.

More **usage examples** [here](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#mistralcli-examples).

---

[top](#table-of-contents)

---

## Token count

Option `-c, --count=SELECT` return count of used tokens and AI response words. SELECT can be *in*, *out*, *total* (in + out), *words*(output words) or any combination, such as `in,out,words`, `in,total`, etc...

**Note**

- **Preview Mode**: `--preview` option don't actually execute any request, therefore the information on output tokens, total tokens and output words cannot be available. Input tokens, the user prompt, are not directly accountable, but the result represents an (anyway good) approximation of real usage.
- **Web Test Mode**: in conjunction with `--web-test` option, `-c, --count=SELECT` option return the actual number of tokens that would be used for a given web page. The count refers to the raw web page, without the addition of messages, such as the *system* message, and other payload stuff. it represents the real web page usage.

More **tokens/words usage examples** [here](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#tokens-and-words-usage).

---

[top](#table-of-contents)

---

## How to get and use Mistral AI Api Key

Get your api key from [Mistral AI](https://console.mistral.ai/api-keys/) site ([pricing](https://docs.mistral.ai/platform/pricing/)).

There are three way to supply api key:

**[1] default**

Create file `.env` and insert the following line:

```bash
MISTRAL_API_KEY='YOUR-API-KEY-HERE'
```

Copy file `.env` to `$HOME/.local/etc/` folder.

**[2] environment variable**

If you prefer, export MISTRAL_API_KEY as environment variable:

```bash
export MISTRAL_API_KEY='YOUR-API-KEY-HERE'
```

**[3] use '-f, --file' option**

You can also supply your own key file, containing the statement `MISTRAL_API_KEY='YOUR-API-KEY-HERE'`, and pass its path as argument to `-f, --file` option.

---

[top](#table-of-contents)

---

## Credits

This project is made possible thanks to the use of the following libraries and the precious work of those who create and maintain them.
Of course thanks also to all those who create and maintain the AI models.

- [godotenv](https://github.com/joho/godotenv)
- [tokenizer](https://github.com/sugarme/tokenizer)
- [getopt](https://github.com/pborman/getopt/)
- [css](https://github.com/ericchiang/css)
- [Mistral AI™](https://mistral.ai/)

---

[top](#table-of-contents)

---

[More **usage examples**](https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#mistralcli---examples)

---

[Others gimlab repos](https://gitlab.com/users/gimaldi/groups)

---
