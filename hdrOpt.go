package main

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/pborman/getopt/v2"
)

type flagRelated struct {
	excluded map[string][]struct{}
	admitted map[string][]struct{}
	name     string
}

type flagDependencies map[string]rune

var admittedCommon map[string][]struct{} = map[string][]struct{}{
	"p":             nil,
	"preview":       nil,
	"response-raw":  nil,
	"response-json": nil,
	"retries":       nil,
	"retries-wait":  nil,
	"timeout":       nil,
	"f":             nil,
	"file":          nil,
	//"status":        nil,
}

var flagNoPrompt flagDependencies = flagDependencies{
	flagPreviewLong:  flagPreviewShort,
	flagToolNameLong: fakeRune,
	//flagToolArgsLong:    fakeRune,
	//flagToolContentLong: fakeRune,
	flagWebTestLong: fakeRune,
}

// how many flags are used
const flagsCount int = 40

// flag labels
const (
	fakeRune                  rune   = '#'
	flagTestTimeoutShort      rune   = fakeRune
	flagTestTimeoutLong       string = "test-timeout"
	flagHelpShort             rune   = fakeRune
	flagHelpLong              string = "help"
	flagVersionShort          rune   = fakeRune
	flagVersionLong           string = "version"
	flagTemperatureShort      rune   = 't'
	flagTemperatureLong       string = "temperature"
	flagTopPShort             rune   = fakeRune
	flagTopPLong              string = "top-p"
	flagFileShort             rune   = 'f'
	flagFileLong              string = "file"
	flagResTokShort           rune   = 'r'
	flagResTokLong            string = "response-tokens"
	flagStreamShort           rune   = fakeRune
	flagStreamLong            string = "stream"
	flagSafeModeShort         rune   = fakeRune
	flagSafeModeLong          string = "safe"
	flagSeedShort             rune   = fakeRune
	flagSeedLong              string = "seed"
	flagListModelsShort       rune   = 'l'
	flagListModelsLong        string = "list-models"
	flagModelShort            rune   = 'm'
	flagModelLong             string = "model"
	flagCountShort            rune   = 'c'
	flagCountLong             string = "count"
	flagAssistantShort        rune   = 'a'
	flagAssistantLong         string = "assistant"
	flagSystemShort           rune   = 's'
	flagSystemLong            string = "system"
	flagWebShort              rune   = fakeRune
	flagWebLong               string = "web"
	flagWebSelectShort        rune   = fakeRune
	flagWebSelectLong         string = "web-select"
	flagWebTestShort          rune   = fakeRune
	flagWebTestLong           string = "web-test"
	flagJsonShort             rune   = fakeRune
	flagJsonLong              string = "response-json"
	flagRawShort              rune   = fakeRune
	flagRawLong               string = "response-raw"
	flagPreviewShort          rune   = 'p'
	flagPreviewLong           string = "preview"
	flagCsvShort              rune   = fakeRune
	flagCsvLong               string = "csv"
	flagPresetShort           rune   = fakeRune
	flagPresetLong            string = "preset"
	flagPresetSystemShort     rune   = fakeRune
	flagPresetSystemLong      string = "preset-system"
	flagListPresetsShort      rune   = fakeRune
	flagListPresetsLong       string = "list-presets"
	flagTimeoutShort          rune   = fakeRune
	flagTimeoutLong           string = "timeout"
	flagTimeoutChunckShort    rune   = fakeRune
	flagTimeoutChunkLong      string = "timeout-chunk"
	flagRetriesshort          rune   = fakeRune
	flagRetriesLong           string = "retries"
	flagRetriesWaitShort      rune   = fakeRune
	flagRetriesWaitLong       string = "retries-wait"
	flagPreviousPromptShort   rune   = fakeRune
	flagPreviousPromptLong    string = "previous-prompt"
	flagEmbedShort            rune   = 'e'
	flagEmbedLong             string = "embed"
	flagResponseFormatShort   rune   = fakeRune
	flagResponseFormatLong    string = "format"
	flagFunctionShort         rune   = fakeRune
	flagFunctionLong          string = "function"
	flagFunctionCallShort     rune   = fakeRune
	flagFunctionCallLong      string = "function-call"
	flagFunctionExamplesShort rune   = fakeRune
	flagFunctionExamplesLong  string = "function-examples"
	flagToolNameShort         rune   = fakeRune
	flagToolNameLong          string = "tool-name"
	flagToolContentShort      rune   = fakeRune
	flagToolContentLong       string = "tool-content"
	flagToolArgsShort         rune   = fakeRune
	flagToolArgsLong          string = "tool-args"
	flagToolCallIdShort       rune   = fakeRune
	flagToolCallIdLong        string = "tool-callid"
	flagDefaultsShort         rune   = fakeRune
	flagDefaultsLong          string = "defaults"
)

// Declare flags and have getopt return pointers to the values.
var (
	flagTestTimeout      bool
	flagHelp             bool
	flagVersion          bool
	flagTemperature      float64
	flagTopP             float64
	flagFile             string
	flagResTok           int
	flagStream           bool
	flagSafeMode         bool
	flagSeed             int
	flagListModels       bool
	flagModel            string
	flagCount            string
	flagAssistant        string
	flagSystem           string
	flagWeb              string
	flagWebSelect        string
	flagWebTest          bool
	flagJson             bool
	flagRaw              bool
	flagPreview          bool
	flagCsv              string
	flagPreset           string
	flagPresetSystem     bool
	flagListPresets      bool
	flagTimeout          int64
	flagTimeoutChunk     int64
	flagRetries          int
	flagRetriesWait      int
	flagPreviousPrompt   string
	flagEmbed            bool
	flagResponseFormat   string
	flagFunction         string
	flagFunctionCall     string
	flagFunctionExamples bool
	flagToolName         string
	flagToolContent      string
	flagToolArgs         string
	flagToolCallId       string
	flagDefaults         bool
	flagRelationsErr     error           = nil
	flags                []getopt.Option = make([]getopt.Option, 0, flagsCount)
	flagsCounter         map[string]int  = make(map[string]int)
	flagsTypes           []string        = make([]string, 0, flagsCount)
)

// init vars at program start with 'init()' internal function
func init() {
	flags = append(flags, getopt.FlagLong(&flagTestTimeout, "test-timeout", '\U0001f405', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTimeout))
	flags = append(flags, getopt.FlagLong(&flagHelp, "help", '\U0001f595', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagHelp))
	flags = append(flags, getopt.FlagLong(&flagVersion, "version", '\U0001f6bd', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagVersion))
	flags = append(flags, getopt.FlagLong(&flagFile, flagFileLong, flagFileShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFile))
	flags = append(flags, getopt.FlagLong(&flagResTok, flagResTokLong, flagResTokShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagResTok))
	flags = append(flags, getopt.FlagLong(&flagTemperature, flagTemperatureLong, flagTemperatureShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTemperature))
	flags = append(flags, getopt.FlagLong(&flagTopP, flagTopPLong, '\U0001f101', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTopP))
	flags = append(flags, getopt.FlagLong(&flagStream, flagStreamLong, '\U0001f598', "").SetGroup("notStream"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagStream))
	flags = append(flags, getopt.FlagLong(&flagSafeMode, flagSafeModeLong, '\U0001f599', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagSafeMode))
	flags = append(flags, getopt.FlagLong(&flagSeed, flagSeedLong, '\U0001f600', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagSeed))
	flags = append(flags, getopt.FlagLong(&flagListModels, flagListModelsLong, flagListModelsShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagListModels))
	flags = append(flags, getopt.FlagLong(&flagModel, flagModelLong, flagModelShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagModel))
	flags = append(flags, getopt.FlagLong(&flagCount, flagCountLong, flagCountShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCount))
	flags = append(flags, getopt.FlagLong(&flagAssistant, flagAssistantLong, flagAssistantShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagAssistant))
	flags = append(flags, getopt.FlagLong(&flagSystem, flagSystemLong, flagSystemShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagSystem))
	flags = append(flags, getopt.FlagLong(&flagWeb, flagWebLong, '\U0001e017', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagWeb))
	flags = append(flags, getopt.FlagLong(&flagWebSelect, flagWebSelectLong, '\U0001e018', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagWebSelect))
	flags = append(flags, getopt.FlagLong(&flagWebTest, flagWebTestLong, '\U0001e019', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagWebTest))
	flags = append(flags, getopt.FlagLong(&flagJson, flagJsonLong, '\U0001f002', "").SetGroup("outputFMT"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagJson))
	flags = append(flags, getopt.FlagLong(&flagRaw, flagRawLong, '\U0001f508', "").SetGroup("outputFMT"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagRaw))
	flags = append(flags, getopt.FlagLong(&flagPreview, flagPreviewLong, flagPreviewShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPreview))
	flags = append(flags, getopt.FlagLong(&flagCsv, flagCsvLong, '\U00010013', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCsv))
	flags = append(flags, getopt.FlagLong(&flagPreset, flagPresetLong, '\U00010014', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPreset))
	flags = append(flags, getopt.FlagLong(&flagListPresets, flagListPresetsLong, '\U00010015', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagListPresets))
	flags = append(flags, getopt.FlagLong(&flagPresetSystem, flagPresetSystemLong, '\U00010016', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPresetSystem))
	flags = append(flags, getopt.FlagLong(&flagTimeout, flagTimeoutLong, '\U0001f521', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTimeout))
	flags = append(flags, getopt.FlagLong(&flagTimeoutChunk, flagTimeoutChunkLong, '\U0001f522', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTimeoutChunk))
	flags = append(flags, getopt.FlagLong(&flagRetries, flagRetriesLong, '\U0001f523', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagRetries))
	flags = append(flags, getopt.FlagLong(&flagRetriesWait, flagRetriesWaitLong, '\U0001f423', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagRetriesWait))
	flags = append(flags, getopt.FlagLong(&flagPreviousPrompt, flagPreviousPromptLong, '\U0001f527', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPreviousPrompt))
	flags = append(flags, getopt.FlagLong(&flagEmbed, flagEmbedLong, flagEmbedShort, "").SetGroup("notStream"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagEmbed))
	flags = append(flags, getopt.FlagLong(&flagResponseFormat, flagResponseFormatLong, '\U00010017', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagResponseFormat))
	flags = append(flags, getopt.FlagLong(&flagFunction, flagFunctionLong, '\U00010018', "").SetGroup("notStream"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFunction))
	flags = append(flags, getopt.FlagLong(&flagFunctionExamples, flagFunctionExamplesLong, '\U0001f510', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFunctionExamples))
	flags = append(flags, getopt.FlagLong(&flagFunctionCall, flagFunctionCallLong, '\U00010019', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFunctionCall))
	flags = append(flags, getopt.FlagLong(&flagToolName, flagToolNameLong, '\U00010020', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagToolName))
	flags = append(flags, getopt.FlagLong(&flagToolContent, flagToolContentLong, '\U00010021', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagToolContent))
	flags = append(flags, getopt.FlagLong(&flagToolArgs, flagToolArgsLong, '\U00010022', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagToolArgs))
	flags = append(flags, getopt.FlagLong(&flagToolCallId, flagToolCallIdLong, '\U0001a022', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagToolCallId))
	flags = append(flags, getopt.FlagLong(&flagDefaults, flagDefaultsLong, '\U00010023', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagDefaults))
}

// due 'getopt' library limitations need the following 'mutually exclusive options' config
func mutuallyExclusiveOptions() error {
	if getopt.GetCount(flagCsvLong) > 0 && flagStream {
		return fmt.Errorf("options --%s and --%s are mutually exclusive", flagStreamLong, flagCsvLong)
	}
	if flagStream && flagEmbed {
		return fmt.Errorf("options --%s and --%s are mutually exclusive", flagStreamLong, flagEmbedLong)
	}
	if (getopt.GetCount(flagCountLong) > 0 || getopt.GetCount(flagCountShort) > 0) && (flagJson || flagRaw) {
		return fmt.Errorf("options --%s and --%s, or --%s, are mutually exclusive", flagCountLong, flagJsonLong, flagRawLong)
	}
	if flagPreview && flagWebTest {
		return fmt.Errorf("options --%s and --%s are mutually exclusive", flagPreviewLong, flagWebTestLong)
	}
	if flagEmbed && (flagPreset != "" || flagToolName != "" || flagToolContent != "" || flagToolArgs != "" || flagAssistant != "" || flagSystem != "" || getopt.GetCount(flagResTokShort) > 0 || getopt.GetCount(flagResTokLong) > 0 || flagResponseFormat != "") {
		return fmt.Errorf("options --%s and --%s, --%s, --%s, --%s, --%s, --%s, --%s or --%s are mutually exclusive", flagEmbedLong, flagToolNameLong, flagToolContentLong, flagToolArgsLong, flagPresetLong, flagAssistantLong, flagSystemLong, flagResTokLong, flagResponseFormatLong)
	}
	if flagFunction != "" {
		excluded := []string{flagResponseFormatLong, flagStreamLong, flagPresetLong}
		if err := relationsHandler(flagFunctionLong, flagFunctionLong, nil, excluded, false); err != nil {
			return err
		}
	}
	/*
		if getopt.GetCount(flagFunctionLong) > 0 && getopt.GetCount(flagResponseFormatLong) > 0 {
			return fmt.Errorf("options --%s and --%s are mutually exclusive", flagFunctionLong, flagResponseFormatLong)
		}
	*/
	if flagAssistant != "" && (flagToolName != "" || flagToolArgs != "" || flagToolContent != "") {
		return fmt.Errorf("options --%s and --%s/--%s/--%s are mutually exclusive", flagAssistantLong, flagToolNameLong, flagToolArgsLong, flagToolContentLong)
	}
	if (flagToolName != "" || flagToolContent != "" || flagToolArgs != "") && flagResponseFormat != "" {
		return fmt.Errorf("options --%s, --%s, --%s and --%s are mutually exclusive", flagToolNameLong, flagToolContentLong, flagToolArgsLong, flagResponseFormatLong)
	}
	return nil
}

// check for flag dependencies (one flag depends from another)
func checkFlagDependencies() error {
	if flagToolName != "" {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagToolCallIdLong] = fakeRune
		if err := dependencies(flagToolNameLong, dependOn); err != nil {
			return err
		}
	}
	if flagToolCallId != "" {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagToolNameLong] = fakeRune
		if err := dependencies(flagToolCallIdLong, dependOn); err != nil {
			return err
		}
	}
	if flagFunctionCall != "" && flagFunction == "" {
		return fmt.Errorf("\aoption '--%s' without '--%s' option OR '--%[2]s' option with wrong arguments", flagFunctionCallLong, flagFunctionLong)
	}
	if (flagWebSelect != "" || flagWebTest) && flagWeb == "" {
		return fmt.Errorf("\aoption '--%s' or '--%s' without '--%s' option", flagWebSelectLong, flagWebTestLong, flagWebLong)
	}
	if flagPresetSystem && flagPreset == "" {
		return fmt.Errorf("\aoption '--%s' without '--%s' option", flagPresetSystemLong, flagPresetLong)
	}
	if getopt.GetCount(flagTimeoutChunkLong) > 0 && !flagStream {
		return fmt.Errorf("\aoption '--%s' without '--%s' option", flagTimeoutChunkLong, flagStreamLong)
	}
	if flagPreviousPrompt != "" && (flagAssistant == "" && (flagToolName == "" && flagToolContent != "" && flagToolArgs != "")) {
		return fmt.Errorf("\aoption '--%s' without '--%s', or '--%s/--%s/--%s' options", flagPreviousPromptLong, flagAssistantLong, flagToolNameLong, flagToolContentLong, flagToolArgsLong)
	}
	if flagToolContent != "" || flagToolName != "" || flagToolArgs != "" {
		if flagToolContent == "" || flagToolName == "" || flagToolArgs == "" {
			return fmt.Errorf("\amissing '--%s', '--%s' or '--%s' option or option argument", flagToolContentLong, flagToolArgsLong, flagToolNameLong)
		}
	}
	if flagAssistant != "" && flagPreviousPrompt == "" {
		return fmt.Errorf("\aoption '--%s' without  '--%s' option", flagAssistantLong, flagPreviousPromptLong)
	}
	if getopt.GetCount(flagRetriesWaitLong) > 0 && getopt.GetCount(flagRetriesLong) == 0 {
		return fmt.Errorf("\aoption '--%s' without  '--%s' option", flagRetriesWaitLong, flagRetriesLong)
	}
	return nil
}

// check if any string was provided to string options
func argProvided() error {
	for i := range flags {
		if flags[i].Seen() && !flags[i].IsFlag() && flags[i].Value().String() == "" {
			return fmt.Errorf("option '--%s' missing or wrong argument", flags[i].LongName())
		}
	}
	return nil
}

// check if long option are given with single or double dash
// without this check long option '--abcd', written with single dash, '-abcd', is parsed as a short option '-a'
// try with 'ls' command: 'ls -l -a' and 'ls -l --author' vs 'ls -l -author' (the latter behave as 'ls -l -a')
func checkLongOptionDashes(args []string) error {
	const dash byte = '-'
	const doubleDash string = "--"
	var wasBool bool = true

	// check if arg begin with dash (if begin with dash could be an option)
loopAllGivenArgs:
	for i := 0; i < len(args); i++ {
		var counter int = 0

		// check for empty strings, such as failed shell expansion - eg: "$(cat non-existant-file)"
		if args[i] == "" {
			continue
		}

		// if previous option was not bool, then next arg is (maybe) only the option argument
		if !wasBool {
			wasBool = true
			continue
		}

		// don't check arguments without dash prefix
		if len(args[i]) > 0 && args[i][0] != dash {
			continue
		}

		// don't check arguments with dash prefix + single char - eg: -a, -b, -c, etc...
		if len(args[i]) == 2 && args[i][0] == dash {
			continue
		}

		// remove single or double dash prefix
		s := strings.TrimPrefix(args[i], string(dash))
		s = strings.TrimPrefix(s, string(dash))

		// remove first '=' char, if any (eg. --my-opt=someData)
		s, _, _ = strings.Cut(s, "=")

		// check if string is a number
		if isNum(s) {
			continue
		}

		for _, v := range flags {
			if v.LongName() == s && (args[i][0] == dash && args[i][1] != dash) {
				// if 'longoption' exist, but was given without double dash (eg: --longoption OK, -longoption WRONG)
				return fmt.Errorf("long option with single dash: '-%s' must be '--%[1]s'", s)
			} else if v.LongName() == s {
				// if option is not boolean, then next arg is not an option
				if !v.IsFlag() {
					wasBool = false
				}
				// 'longoption' exist and was given correctly (eg: --longoption)
				continue loopAllGivenArgs
			} else {
				// 'longoption' not found in this inner loop cycle
				counter++
			}
		}

		// option not found
		if counter >= len(flags) {
			return fmt.Errorf("wrong option: '%s'", args[i])
		}
	}
	return nil
}

// used by presets - check if any parameter, such as temperature or model, was given.
// If yes it use command-line parameter setting, instead of preset parameter setting.
func countFlags(args []string, flag string) bool {
	for _, v := range args {
		flagsCounter[v]++
	}
	if _, ok := flagsCounter[flag]; ok {
		return true
	}
	return false
}

// check 'string type' option arguments for "-" char as first char of argument
func checkFlagTypeString() error {
	for i := range flags {
		if flags[i].Seen() && (flagsTypes[i] == "string" && strings.HasPrefix(flags[i].Value().String(), "-")) {
			return fmt.Errorf("\awrong parameter for %s: '%s'", flags[i].Name(), flags[i].Value().String())
		}
	}
	return nil
}

// check if string is a number, integer or float - false = NOT number, true = IS number
func isNum(s string) bool {
	if _, err := strconv.ParseInt(s, 10, 64); err == nil {
		return true
	}
	if _, err := strconv.ParseFloat(s, 64); err == nil {
		return true
	}
	if _, err := strconv.ParseComplex(s, 128); err == nil {
		return true
	}
	return false
}

// callback function for 'getopt.Visit()': ADMITTED
// ref: https://pkg.go.dev/github.com/pborman/getopt/v2#Visit
func checkFlagRelationsAdmitted(option getopt.Option, fr *flagRelated) {
	n := option.Name()
	s := strings.TrimPrefix(n, "-")
	s = strings.TrimPrefix(s, "-")
	if fr.admitted != nil {
		if _, ok := fr.admitted[s]; !ok {
			flagRelationsErr = fmt.Errorf("options '--%s' and '%s' are mutually exclusive", fr.name, n)
		}
	}
}

// callback function for 'getopt.Visit()': EXCLUDED
// ref: https://pkg.go.dev/github.com/pborman/getopt/v2#Visit
func checkFlagRelationsExcluded(option getopt.Option, fr *flagRelated) {
	n := option.Name()
	s := strings.TrimPrefix(n, "-")
	s = strings.TrimPrefix(s, "-")
	if fr.excluded != nil {
		if _, ok := fr.excluded[s]; ok {
			flagRelationsErr = fmt.Errorf("options '--%s' and '%s' are mutually exclusive", fr.name, n)
		}
	}
}

// wrapper for the 'getopt.Visit()' callback function
// ref: https://pkg.go.dev/github.com/pborman/getopt/v2#Visit
func wrapCheckFlagRelations(callback func(getopt.Option, *flagRelated), fr *flagRelated) func(getopt.Option) {
	return func(option getopt.Option) {
		callback(option, fr)
	}
}

// check admitted/excluded flags for a given flag name using 'getopt.Visit()'
// ref: https://pkg.go.dev/github.com/pborman/getopt/v2#Visit
func relationsHandler(name, flagLongName string, admitted, excluded []string, checkNonOptArgs bool) error {
	// non-option arguments
	if checkNonOptArgs {
		if len(getopt.Args()) > 0 {
			s := getopt.Args()
			return fmt.Errorf("option '--%s' and arg %q are mutually exclusive", flagLongName, s[0])
		}
	}
	// init flag data
	f := &flagRelated{
		admitted: map[string][]struct{}{
			name: nil,
		},
		excluded: map[string][]struct{}{},
		name:     name,
	}
	// admitted
	if admitted != nil {
		for _, v := range admitted {
			f.admitted[v] = nil
		}
		for k := range admittedCommon {
			f.admitted[k] = nil
		}
		// check relations
		relations := wrapCheckFlagRelations(checkFlagRelationsAdmitted, f)
		getopt.Visit(relations)
	}
	// excluded
	if excluded != nil && len(excluded) > 0 {
		for _, v := range excluded {
			f.excluded[v] = nil
		}
		// check relations
		relations := wrapCheckFlagRelations(checkFlagRelationsExcluded, f)
		getopt.Visit(relations)
	}
	// result
	if flagRelationsErr != nil {
		return flagRelationsErr
	}
	return nil
}

// check a given flag's dependencies
func dependencies(flagName string, dependOn flagDependencies) error {
	for l, s := range dependOn {
		if getopt.GetCount(l) == 0 {
			switch {
			case s != fakeRune:
				if getopt.GetCount(s) == 0 {
					return fmt.Errorf("%s", dependenciesError(flagName, dependOn))
				}
				return nil
			default:
				return fmt.Errorf("%s", dependenciesError(flagName, dependOn))
			}
		}
	}
	return nil
}

// print a given flag's dependencies
func dependenciesError(flagName string, dependOn flagDependencies) string {
	var b strings.Builder
	var i int = 0
	var l int = len(dependOn)
	if _, err := b.WriteString("\aoption '--" + flagName + "' depend from options "); err != nil {
		return err.Error()
	}
	for f := range dependOn {
		s := fmt.Sprintf("'--%s'", f)
		if _, err := b.WriteString(s); err != nil {
			return err.Error()
		}
		if l > 1 && (l-i) == 2 {
			if _, err := b.WriteString(" and "); err != nil {
				return err.Error()
			}
			i++
			continue
		}
		if (l - i) > 2 {
			if _, err := b.WriteString(", "); err != nil {
				return err.Error()
			}
		}
		i++
	}
	return b.String()
}

// checks for flags that do not require any prompts
func checkNoPrompt() bool {
	for l, s := range flagNoPrompt {
		switch {
		case s != fakeRune:
			if getopt.GetCount(l) > 0 || getopt.GetCount(s) > 0 {
				return true
			}
		default:
			if getopt.GetCount(l) > 0 {
				return true
			}
		}
	}
	return false
}
