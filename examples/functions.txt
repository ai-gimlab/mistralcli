# --- function "helpdesk_request" --- #
export F0='{
  "name": "helpdesk_request",
  "description": "send a support request email to a specific HelpDesk department",
  "parameters": {
    "type": "object",
    "properties": {
      "email": {
        "type": "string",
        "enum": [
          "network@helpdesk.local",
          "hardware@helpdesk.local",
          "email@helpdesk.local",
          "booking@helpdesk.local",
          "other@helpdesk.local"
        ],
        "description": "email address for specific helpdesk departments, you must choose the right email address based on user request."
      },
      "subject": {
        "type": "string",
        "description": "short email subject, based on user request"
      },
      "body": {
        "type": "string",
        "description": "you must rewrite the user request as bulleted list of all problems and requests"
      }
    },
    "required": [
      "email",
      "subject",
      "body"
    ]
  }
}'

# --- function "time_in_italy" --- #
export F1='{
  "name": "time_in_italy",
  "description": "It takes the current time of the Italian office",
  "parameters": {
    "type": "object"
  }
}'

# --- function "retrieve_ticket_id_status" --- #
export F2='{
  "name": "retrieve_ticket_id_status",
  "description": "Check the status of a support request for a given ticket id.",
  "parameters": {
    "type": "object",
    "properties": {
      "ticket_id": {
        "type": "string",
        "description": "The ticket id."
      }
    },
    "required": ["ticket_id"]
  }
}'

# --- message "SYSTEM" model function call --- #
export SYSTEM="You are provided with the following function: helpdesk_request, time_in_italy, retrieve_ticket_id_status.
If the user requests more than one information a function must be called multiple times.
For example given the user question:
'I have a problem with my printer. Paper is stuck inside the printer.
Also, I can't connect to any company file server. Can you help me?'
You must call 'helpdesk_request' two times. One for the printer, so hardware support,
the other for file servers related problems, so network support.
If you choose to call one or more functions, use the function call, without adding notes or comments."

# --- message "SYSTEM" final response --- #
export SYSRESP="You are 'Support Team', your signature 'Support Team'. Your answers must be in a formal style, markdown formatted."

# --- message "USER" --- #
export PROMPT="Hello,
I have to call the offices of the Italian headquarters. What time is it now in Italy?

I also have a couple of problems, one with my computer monitor, it doesn't show anything.
It turns on, emits a short flash then turns black.
And my office colleagues are reporting that they can't access the production servers.

Finally I need to know how long it takes to resolve my previous support request, ticket ID SRVVLG5E."

# --- tool content --- #
export IT_TIME='{"time": "15:49", "office_status": "OPEN"}'
export HD1='{"ticket_id": "IWEA5PC4", "message": "Your problem has been assigned to a team", "dep": "hw", "reference_email": "hardware@support.local"}'
export HD2='{"ticket_id": "F3U24N5P", "message": "Your problem has been assigned to a team", "dep": "network", "reference_email": "network@support.local"}'
export TKT='{"ticket_id": "SRVVLG5E", "status": "Waiting for the replacement part", "estimated_time": "1 week"}'

# --- tool name --- #
# double comma
export TOOL="time_in_italy,,helpdesk_request,,helpdesk_request,,retrieve_ticket_id_status"
# or
# single comma
#export TOOL="time_in_italy,helpdesk_request,helpdesk_request,retrieve_ticket_id_status"

# --- tool call id --- #
# double comma
export TOOL_ID="iowa5FzYz,,9sWmHE7Ar,,t71XTdIM8,,5dQBQSUai"
# or
# single comma
#export TOOL_ID="iowa5FzYz,9sWmHE7Ar,t71XTdIM8,5dQBQSUai"

# --- tool args --- #
export ARGS0=""
export ARGS1='{"email": "hardware@helpdesk.local","subject": "Computer monitor not working","body": "- Monitor turns on but does not display anything\n- Monitor emits a short flash then turns black"}'
export ARGS2='{"email": "network@helpdesk.local","subject": "Colleagues cannot access production servers","body": "- Colleagues reporting inability to access production servers"}'
export ARGS3='{"ticket_id": "SRVVLG5E"}'


# ----------------------------- #
: <<'END_COMMENT'

# --- model function call --- #
function call: yes

function name: time_in_italy
function args: null

function name: helpdesk_request
function args:
{
  "email": "hardware@helpdesk.local",
  "subject": "Computer monitor not working",
  "body": "- Monitor turns on but does not display anything\n- Monitor emits a short flash then turns black"
}

function name: helpdesk_request
function args:
{
  "email": "network@helpdesk.local",
  "subject": "Colleagues cannot access production servers",
  "body": "- Colleagues reporting inability to access production servers"
}

function name: retrieve_ticket_id_status
function args:
{
  "ticket_id": "SRVVLG5E"
}

---

# --- commands --- #

# function call
mistralcli --function "$F0,,$F1,,$F2" --system "$SYSTEM" "$PROMPT"

# tool completion
mistralcli --temperature 0.7 --tool-name "$TOOL" --tool-args "$ARGS0,,$ARGS1,,$ARGS2,,$ARGS3" --tool-content "$IT_TIME,,$HD1,,$HD2,,$TKT" --system "$SYSRESP" --previous-prompt "$PROMPT"

# no args tool syntax examples
--tool-args ",,$ARGS1,,$ARGS2,,$ARGS3"
--tool-args "'',,$ARGS1,,$ARGS2,,$ARGS3"
--tool-args """,,$ARGS1,,$ARGS2,,$ARGS3"
--tool-args "$ARGS0,,$ARGS1,,,,$ARGS3"
--tool-args "$ARGS0,,$ARGS1,,'',,$ARGS3"
--tool-args "$ARGS0,,$ARGS1,,"",,$ARGS3"

---

Ref: https://gitlab.com/ai-gimlab/mistralcli/-/tree/main/examples?ref_type=heads#functions

END_COMMENT
# ----------------------------- #

