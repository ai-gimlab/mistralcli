package main

import (
	"context"
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	tokenizer "github.com/sugarme/tokenizer"
	"github.com/sugarme/tokenizer/pretrained"
)

// check if user given model name is a valid model name
func checkModelName(name string) error {
	// function name to return if any error occur
	var funcName string = "checkModelName"

	if err := listModels(); err != nil {
		return err
	}

	// check against registered names
	for _, modelName := range modelNames {
		if modelName == name {
			return nil
		}
	}

	// if not found, return error
	return fmt.Errorf("func %q - wrong model name: %q", funcName, name)
}

// count input tokens in preview mode and web-test mode
func countEstimatedInputTokens() {
	// function name to return if any error occur
	var funcName string = "countEstimatedInputTokens"

	// download and cache pretrained tokenizer
	configFile, err := tokenizer.CachedPath(modelTokenizer, tokenizerConfig)
	if err != nil {
		err = fmt.Errorf("%q: %v", "tokenizer.CachedPath", err)
		fmt.Printf("%v\n", fmt.Errorf("func %q - error counting input tokens: %w\n", funcName, err))
		return
	}

	// create tokenizer encoder
	tke, err := pretrained.FromFile(configFile)
	if err != nil {
		err = fmt.Errorf("%q: %v", "pretrained.FromFile", err)
		fmt.Printf("%v\n", fmt.Errorf("func %q - error counting input tokens: %w\n", funcName, err))
		return
	}

	// calculate offset (very empirically)
	var offset int = defaultInputMessageOffset
	if flagSystem != "" || flagPreview || flagPreset != "" || flagWeb != "" || flagAssistant != "" {
		offset += defaultInSysAssistMsgOffset
	}

	// system PROMPT
	var systemPrompt string
	switch {
	case flagWebTest:
		// estimate only web page raw token usage
		en, err := tke.EncodeSingle(webPageContent)
		if err != nil {
			err = fmt.Errorf("%q: %v", "tke.EncodeSingle", err)
			fmt.Printf("%v\n", fmt.Errorf("func %q - error counting input tokens: %w\n", funcName, err))
			return
		}
		estimatedInputTokens = len(en.Tokens)
		return
	case flagSystem != "":
		systemPrompt = flagSystem
	case flagPreset != "":
		systemPrompt = presets[flagPreset].systemMessage
	case flagWeb != "":
		systemPrompt = systemMessageWeb
	}

	// result
	en, err := tke.EncodeSingle(prompt + systemPrompt + flagAssistant)
	if err != nil {
		err = fmt.Errorf("%q: %v", "tke.EncodeSingle", err)
		fmt.Printf("%v\n", fmt.Errorf("func %q - error counting input tokens: %w\n", funcName, err))
		return
	}
	estimatedInputTokens = len(en.Tokens) + offset
}

// tokens usage
func printTokensUsage(r *response, re *responseEmbed) {
	// function name to return if any error occur
	var funcName string = "printTokensUsage"

	// Set a read timeout for tokenizer
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	switch {
	case (flagPreview && flagEmbed) || flagPreview || flagWebTest:
		// estimated input tokens
		if !strings.Contains(flagCount, tokenInput) && !flagEmbed {
			fmt.Printf("\a\n'--%s' or '--%s' and '-%c' or '--%s' provided: %q tokens not available\n", flagPreviewLong, flagWebTestLong, flagCountShort, flagCountLong, flagCount)
			return
		}
		for ctxDone := true; ctxDone; {
			select {
			case <-ctx.Done():
				fmt.Printf("func %q - timeout counting input tokens\n", funcName)
				ctxDone = false
			default:
				if estimatedInputTokens > 0 {
					if flagWebTest {
						fmt.Printf("\nestimated web page tokens: %d\n", estimatedInputTokens)
					} else {
						fmt.Printf("\nestimated input tokens: %d\n", estimatedInputTokens)
					}
					ctxDone = false
				}
			}
		}
	case flagEmbed:
		// embeddings
		fmt.Printf("input tokens:\t%d\n", re.Usage.PromptTokens)
	case flagCount == tokenInput:
		// single argument (any of 'in', 'out', 'total', 'words')
		fmt.Printf("%d\n", r.Usage.PromptTokens)
	case flagCount == tokenOutput:
		fmt.Printf("%d\n", r.Usage.CompletionTokens)
	case flagCount == tokenSumInOut:
		fmt.Printf("%d\n", r.Usage.TotalTokens)
	case flagCount == outputWords:
		fmt.Printf("%d\n", outputWordsCount)
	case strings.Contains(flagCount, ","):
		// --- multiple arguments (a combination of 'in', 'out', 'total', 'words')
		if strings.Contains(flagCount, tokenInput) {
			fmt.Printf("input tokens:\t%d\n", r.Usage.PromptTokens)
		}
		if strings.Contains(flagCount, tokenOutput) {
			fmt.Printf("output tokens:\t%d\n", r.Usage.CompletionTokens)
		}
		if strings.Contains(flagCount, tokenSumInOut) {
			fmt.Printf("total tokens:\t%d\n", r.Usage.TotalTokens)
		}
		if strings.Contains(flagCount, outputWords) {
			fmt.Printf("output words:\t%d\n", outputWordsCount)
		}
	}
}

// export csv data
func writeFileCSV(newFile bool) error {
	// function name to return if any error occur
	var funcName string = "writeFileCSV"

	// create or open file (append)
	f, err := os.OpenFile(flagCsv, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return fmt.Errorf("func %q - file %s: %w\n", funcName, flagCsv, err)
	}
	defer f.Close()

	// check file size
	fileInfo, err := f.Stat()
	if err != nil {
		return fmt.Errorf("func %q - error checking csv file size: %w\n", funcName, err)
	}

	// prepare data
	var b strings.Builder
	if newFile || fileInfo.Size() == 0 {
		// if csv file does not exist, or is empty, write headers
		if _, err := b.WriteString(csvFieldsName); err != nil {
			return fmt.Errorf("func %q - error composing csv [header]: %w\n", funcName, err)
		}
	}

	// append row
	for i := 0; i < len(csvFields); i++ {
		if _, err := b.WriteString(csvFields[i]); err != nil {
			return fmt.Errorf("func %q - error composing csv [data]: %w\n", funcName, err)
		}
		if i < len(csvFields)-1 {
			if _, err := b.WriteString(","); err != nil {
				return fmt.Errorf("func %q - error composing csv [data]: %w\n", funcName, err)
			}
		}
	}

	// end row with newline
	if _, err := b.WriteString("\n"); err != nil {
		return fmt.Errorf("func %q - error composing csv [data]: %w\n", funcName, err)
	}

	// write data to csv file
	if _, err := f.Write([]byte(b.String())); err != nil {
		return fmt.Errorf("func %q - file %s: %w\n", funcName, flagCsv, err)
	}

	return nil
}

// encode request data to csv
func requestCSV(req *request) {
	// user, system, assistant PROMPTs
	if len(req.Messages) > 0 {
		for i := range req.Messages {
			// sanitize message: all in a single line
			s := req.Messages[i].Content
			s = strings.ReplaceAll(s, "\n", "\\n")
			switch {
			case req.Messages[i].Role == system:
				csvFields[requestContentSystem] = "\"" + s + "\""
			case req.Messages[i].Role == user:
				csvFields[requestContentUser] = "\"" + s + "\""
			}
		}
	}

	// request parameters
	switch {
	case req.ResponseFormat != nil:
		csvFields[requestResponseFormat] = req.ResponseFormat.Type
	default:
		csvFields[requestResponseFormat] = "null"
	}
	csvFields[requestModel] = req.Model
	csvFields[requestTemperature] = fmt.Sprintf("%.2f", req.Temperature)
	csvFields[requestTop_p] = fmt.Sprintf("%.2f", req.TopP)
	csvFields[requestSafePrompt] = fmt.Sprintf("%t", req.SafeMode)
	switch {
	case seed:
		csvFields[requestSeed] = fmt.Sprintf("%d", req.RandomSeed)
	default:
		csvFields[requestSeed] = fmt.Sprintf("")
	}
}

// encode response data to csv
func responseCSV(resp response) {
	csvFields[responseModel] = resp.Model
	csvFields[responseInputTokens] = fmt.Sprintf("%d", resp.Usage.PromptTokens)
	csvFields[responseOutputTokens] = fmt.Sprintf("%d", resp.Usage.CompletionTokens)
	csvFields[responseTotalTokens] = fmt.Sprintf("%d", resp.Usage.TotalTokens)
	csvFields[responseOutputWords] = fmt.Sprintf("%d", outputWordsCount)

	// sanitize response message: all in a single line, remove extra quotes
	s := resp.Choices[0].Message.Content
	s = strings.ReplaceAll(s, "\n", "\\n")
	s = strings.ReplaceAll(s, "\"", "")
	csvFields[responseContent] = "\"" + s + "\""
}

func embeddingsCSV(re *responseEmbed) error {
	// function name to return if any error occur
	var funcName string = "embeddingsCSV"

	// last row id of existing csv file - used for append data
	var embedCsvFileOffset int = 0

	// use custom csv file
	if flagCsv != "" {
		embedCsvFile = flagCsv
	}

	// create or open file (append)
	f, err := os.OpenFile(embedCsvFile, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0600)
	if err != nil {
		return fmt.Errorf("func %q - error creating embeddings csv file: %w\n", funcName, err)
	}
	defer f.Close()

	// check file size
	fileInfo, err := f.Stat()
	if err != nil {
		return fmt.Errorf("func %q - error checking csv file size: %w\n", funcName, err)
	}

	// Create a CSV writer
	csvWriter := csv.NewWriter(f)

	// Create a CSV reader
	csvReader := csv.NewReader(f)

	// if new file: write headers
	// else: check last row id
	switch {
	case fileInfo.Size() == 0:
		// if csv file does not exist, or is empty, write headers
		if err := csvWriter.Write(embedCsvHeader); err != nil {
			return fmt.Errorf("func %q - error writing headers to csv file %s: %w\n", funcName, embedCsvFile, err)
		}
	case fileInfo.Size() != 0:
		var records [][]string
		for {
			record, err := csvReader.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				return fmt.Errorf("func %q - error reading csv file %s: %w\n", funcName, embedCsvFile, err)
			}

			records = append(records, record)
		}
		if offset, err := strconv.ParseInt(records[len(records)-1][0], 10, 64); err != nil {
			return fmt.Errorf("func %q - error reading last line of csv file %s: %w\n", funcName, embedCsvFile, err)
		} else {
			embedCsvFileOffset = int(offset) + 1
		}
	}

	for i, v := range re.Data {
		var embedding strings.Builder
		id := fmt.Sprintf("%d", embedCsvFileOffset)
		text := inputs[i]
		if _, err := embedding.WriteString("["); err != nil {
			return fmt.Errorf("func %q - error creating embeddings CSV row: %w\n", funcName, err)
		}
		for i, e := range v.Embedding {
			f := strconv.FormatFloat(e, 'f', -1, 64)
			trimmed := strings.TrimRight(strings.TrimRight(f, "0"), ".")
			if _, err := embedding.WriteString(trimmed); err != nil {
				return fmt.Errorf("func %q - error creating embeddings CSV field: %w\n", funcName, err)
			}
			if i < len(v.Embedding)-1 {
				if _, err := embedding.WriteRune(','); err != nil {
					return fmt.Errorf("func %q - error creating embeddings CSV field: %w\n", funcName, err)
				}
			}
		}
		if _, err := embedding.WriteRune(']'); err != nil {
			return fmt.Errorf("func %q - error creating embeddings CSV row: %w\n", funcName, err)
		}

		// Write the data record to the CSV file.
		if err := csvWriter.Write([]string{id, text, embedding.String()}); err != nil {
			return fmt.Errorf("func %q - error writing data to CSV file %s: %w\n", funcName, embedCsvFile, err)
		}
		embedCsvFileOffset++
	}

	// Flush the CSV writer.
	csvWriter.Flush()

	fmt.Printf("\nembeddings data successfully written to file %q\n\n", embedCsvFile)

	return nil
}

func finalResult(bodyString string, r *response, re *responseEmbed) error {
	// function name to return if any error occur
	var funcName string = "finalResult"

	// --- parse response --- //
	// raw response
	if flagRaw {
		fmt.Printf("%s\n", bodyString)
		return nil
	}
	// json response
	if flagJson {
		ppJSON, err := prettyPrintJsonString(bodyString)
		if err != nil {
			return fmt.Errorf("func %q - error decoding JSON data: %w\n", funcName, err)
		}
		fmt.Printf("%s\n", ppJSON)
		return nil
	}
	// save embeddings
	if flagEmbed {
		if err := embeddingsCSV(re); err != nil {
			return err
		}
		return nil
	}
	// print results
	if len(r.Choices) > 0 {
		for _, v := range r.Choices {
			// JSON output format requested
			if responseFormat == "json" || responseFormat == "json_object" {
				ppJSON, err := prettyPrintJsonString(v.Message.Content)
				if err != nil {
					return fmt.Errorf("func %q - error decoding JSON data: %w\n", funcName, err)
				}
				fmt.Printf("%s\n", ppJSON)
				continue
			}
			// function call YES
			if flagFunction != "" && (v.FinishReason == functionCallFinishReason || v.Message.ToolCalls != nil) {
				fmt.Printf("function call: yes\n")
				// print function results
				for i, t := range v.Message.ToolCalls {
					fmt.Printf("\nfunction name: %s\n", t.Function.Name)
					fmt.Printf("function id:   %s\n", t.Id)
					if t.Function.Arguments == "{}" {
						fmt.Printf("function args: null")
					}
					if t.Function.Arguments != "{}" {
						ppJSON, err := prettyPrintJsonString(t.Function.Arguments)
						if err != nil {
							return fmt.Errorf("func %q - error decoding JSON data: %w\n", funcName, err)
						}
						fmt.Printf("function args:\n")
						fmt.Printf("%s", ppJSON)
					}
					if i < len(v.Message.ToolCalls)-1 {
						fmt.Println()
					}
				}
				continue
			}
			if flagFunction != "" && v.FinishReason != functionCallFinishReason {
				// function call NO
				fmt.Printf("function call: no\n\n")
				fmt.Printf("%s\n", v.Message.Content)
				continue
			}
			// normal output format
			fmt.Printf("%s\n", v.Message.Content)
		}
		fmt.Println()
	}
	return nil
}
