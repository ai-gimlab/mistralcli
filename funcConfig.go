package main

import (
	"errors"
	"fmt"
	"io/fs"
	"os"
	"strings"
	"time"

	"github.com/pborman/getopt/v2"
)

func configFlags() error {
	// function name to return if any error occur
	var funcName string = "configFlags"

	// model selection (if requested, otherwise use default model)
	if (getopt.GetCount(flagModelLong) > 0 || getopt.GetCount(flagModelShort) > 0) && flagModel != "" {
		// check if model exist
		if err := checkModelName(flagModel); err != nil {
			return err
		}
		// config model
		switch {
		case flagEmbed:
			// model embedding
			modelEmbed = flagModel
		case flagFunction != "":
			// model function
			modelFunction = flagModel
		case flagToolName != "":
			modelTool = flagModel
		default:
			// model language
			model = flagModel
		}
	}

	if getopt.GetCount(flagEmbedLong) > 0 || getopt.GetCount(flagEmbedShort) > 0 {
		inputs = strings.Split(prompt, objectSplitChar)
		return nil
	}
	/*
		if flagTestTimeout {
			url = urlCompletionTimeoutTest0
		}
	*/
	if flagStream {
		timeout = timeoutCompletionStream
	}
	if flagTimeout != 0 {
		timeout = time.Second * time.Duration(flagTimeout)
	}
	if getopt.GetCount(flagTimeoutChunkLong) > 0 {
		timeoutChunk = time.Second * time.Duration(flagTimeoutChunk)
	}
	if getopt.GetCount(flagRetriesLong) > 0 {
		retries = flagRetries
	}
	if getopt.GetCount(flagRetriesWaitLong) > 0 {
		retriesWait = flagRetriesWait
	}
	if flagFunction != "" {
		functionCall = "auto"
		functionList = strings.Split(flagFunction, objectSplitChar)
	}
	if flagFunctionCall != "" {
		functionCall = flagFunctionCall
	}
	if flagToolName != "" {
		s := strings.ReplaceAll(flagToolName, objectSplitChar, ",")
		toolNameList = strings.Split(s, ",")
	}
	if flagToolCallId != "" {
		s := strings.ReplaceAll(flagToolCallId, objectSplitChar, ",")
		toolCallIdList = strings.Split(s, ",")
	}
	if flagToolContent != "" {
		toolContentList = strings.Split(flagToolContent, objectSplitChar)
	}
	if flagToolArgs != "" {
		toolArgsList = strings.Split(flagToolArgs, objectSplitChar)
	}
	if len(toolNameList) > 0 {
		if i := ((len(toolArgsList) == len(toolNameList)) && (len(toolContentList) == len(toolNameList))); !i {
			return fmt.Errorf("func %q - the number of %s, %d, differs from the number of --%s, %d, or number of --%s, %d", funcName, flagToolNameLong, len(toolNameList), flagToolContentLong, len(toolContentList), flagToolArgsLong, len(toolArgsList))
		}
	}
	if (getopt.GetCount(flagTemperatureLong) > 0 || getopt.GetCount(flagTemperatureShort) > 0) && (flagTemperature >= minLimitDefault && flagTemperature <= maxTemperatureLimit) {
		temperature = flagTemperature
	}
	if (getopt.GetCount(flagTemperatureLong) > 0 || getopt.GetCount(flagTemperatureShort) > 0) && (flagTemperature < minLimitDefault || flagTemperature > maxTemperatureLimit) {
		return fmt.Errorf("func %q - wrong parameter for -%c or --%s: %.2f (less than %.2f or greater than %.2f)", funcName, flagTemperatureShort, flagTemperatureLong, flagTemperature, minLimitDefault, maxTemperatureLimit)
	}
	if getopt.GetCount(flagTopPLong) > 0 && (flagTopP >= minLimitDefault && flagTopP <= maxTopPLimit) {
		top_p = flagTopP
	}
	if getopt.GetCount(flagTopPLong) > 0 && (flagTopP < minLimitDefault || flagTopP > maxTopPLimit) {
		return fmt.Errorf("func %q - wrong parameter for --%s: %.2f (less than %.2f or greater than %.2f)", funcName, flagTopPLong, flagTopP, minLimitDefault, maxTopPLimit)
	}
	if flagCount != "" {
		if !regexCompileCountOption.MatchString(flagCount) {
			return fmt.Errorf("wrong argument for option -%c or option --%s: %s", flagCountShort, flagCountLong, flagCount)
		}
	}
	if flagResponseFormat != "" {
		s := strings.ToLower(flagResponseFormat)
		switch {
		case s != "text" && s != "json" && s != "json_object":
			return fmt.Errorf("wrong argument for option --%s: %s", flagResponseFormatLong, flagResponseFormat)
		default:
			if s == "json" {
				s = "json_object"
			}
			responseFormat = s
		}
	}
	if flagPreset != "" {
		var p presetSettings
		if err := p.set(flagPreset); err != nil {
			return err
		}
	}
	if flagWeb != "" {
		if selected, err := selectHTML(flagWeb, flagWebSelect); err != nil {
			return err
		} else {
			webPageContent = selected
		}
	}
	if flagCsv != "" && !flagPreview {
		// if csv file does not exist ask permission to create a new one
		var r rune
		_, errFileNotExist := os.Open(flagCsv)
		if newFileCSV = errors.Is(errFileNotExist, fs.ErrNotExist); newFileCSV {
			// ask to confirm new file creation
			fmt.Printf("File %q does not exist. Create new one (%q to quit and exit)? [y/n/q]: ", flagCsv, "q")
			if _, err := fmt.Scanf("%c", &r); err != nil {
				return fmt.Errorf("func %q - error creating CSV file %q: %w", funcName, flagCsv, err)
			}
			switch {
			case r == 'y' || r == 'Y':
				newFileCSV = true
			case r == 'n' || r == 'N':
				flagCsv = ""
			case r == 'q' || r == 'Q':
				return (fmt.Errorf("EXIT"))
			default:
				return fmt.Errorf("wrong option: %c\nexit program", r)
			}
		}
	}
	if getopt.GetCount(flagResTokLong) > 0 || getopt.GetCount(flagResTokShort) > 0 {
		maxTokens = flagResTok
	}
	if getopt.GetCount(flagSeedLong) > 0 {
		seed = true
	}
	return nil
}

func configRoles() {
	// ROLE: system
	if flagSystem != "" || flagPreset != "" || flagWeb != "" || flagFunction != "" {
		var systemMessage message
		systemMessage.Role = system
		switch {
		case flagPreset != "" && flagSystem == "":
			systemMessage.Content = presets[flagPreset].systemMessage
		case flagWeb != "" && flagSystem == "":
			systemMessage.Content = systemMessageWeb
		default:
			s := strings.ReplaceAll(flagSystem, "\\n", "\n")
			systemMessage.Content = s
		}
		messages = append(messages, systemMessage)
	}

	// ROLE: tool
	if flagToolName != "" {
		var assistantMessage message
		var userMessage message
		//var calls toolCalls
		userMessage.Role = user
		userMessage.Content = flagPreviousPrompt
		messages = append(messages, userMessage)
		assistantMessage.Role = assistant
		assistantMessage.Content = ""
		for i, n := range toolNameList {
			var f function
			var t toolCalls
			f.Name = n
			f.Arguments = toolArgsList[i]
			t.Function = f
			t.Id = toolCallIdList[i]
			assistantMessage.ToolCalls = append(assistantMessage.ToolCalls, t)
		}
		messages = append(messages, assistantMessage)

		for i, n := range toolNameList {
			var toolMessage message
			toolMessage.Role = tool
			toolMessage.Name = n
			toolMessage.ToolCallId = toolCallIdList[i]
			if flagToolContent != "" {
				toolMessage.Content = toolContentList[i]
			}
			messages = append(messages, toolMessage)
		}
		return
	}

	// ROLE: assistant
	if flagAssistant != "" {
		var userMessage message
		userMessage.Role = user
		userMessage.Content = flagPreviousPrompt
		messages = append(messages, userMessage)
		var assistantMessage message
		assistantMessage.Role = assistant
		assistantMessage.Content = flagAssistant
		messages = append(messages, assistantMessage)
	}

	// ROLE: user
	var userMessage message
	userMessage.Role = user
	userMessage.Content = prompt
	messages = append(messages, userMessage)
}

func userPromptExceptions() error {
	// function name to return if any error occur
	var funcName string = "userPromptExceptions"

	var b strings.Builder
	var writeErr []error = make([]error, 0)
	switch {
	case flagWebTest:
		prompt = webPageContent
		return nil
	case flagPreset != "" && flagWeb != "":
		_, err := b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(webPageContent)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		prompt = b.String()
	case flagPreset != "":
		_, err := b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(prompt)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		prompt = b.String()
	case flagWeb != "":
		_, err := b.WriteString(delimiterWeb)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(webPageContent)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiterWeb)
		writeErr = append(writeErr, err)
		_, err = b.WriteString("\n\n")
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(prompt)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		prompt = b.String()
	}
	if len(writeErr) > 0 {
		for _, e := range writeErr {
			if e != nil {
				return fmt.Errorf("func %q - error composing web prompt: %w", funcName, e)
			}
		}
	}
	return nil
}
